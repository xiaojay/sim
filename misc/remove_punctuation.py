#coding=utf-8
import os, time, datetime, json, django
os.environ['DJANGO_SETTINGS_MODULE'] = 'sim.settings'
from sim.models import *
punctuation = [
    '.', ',', '?', '!', ';', '&', '*', '%', '+', '=', '-', '~', '$' , '(', ')', '@', '^', '"', '>', '_',
    u'，', u'。', u'！', u'？', u'：', u'‘', u'“', u'”'
]
i = 0
for d in Dialog.objects.all():
    q = d.query
    
    while True:
        if q and q[-1] in punctuation:
            q = q[:-1]
        else:
            break
    
    while True:
        if q and q[0] in punctuation:
            q = q[1:]
        else:
            break
    
    if not q:
        continue

    if d.query != q:
        i += 1
        if i%1000 == 0:print i
        d.query = q
        try: 
            d.save()
        except django.db.utils.IntegrityError:
            continue
print i
