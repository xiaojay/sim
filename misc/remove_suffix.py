#coding=utf-8
import os, time, datetime, json, django
os.environ['DJANGO_SETTINGS_MODULE'] = 'sim.settings'
from sim.models import *
suffix = [u'啊', u'阿', u'呀', u'啦', u'吧', u'把', u'巴']
i = 0
for d in Dialog.objects.all():
    q = d.query
    
    while True:
        if q and q[-1] in suffix:
            q = q[:-1]
        else:
            break
    
    if not q:
        continue

    if d.query != q:
        i += 1
        if i%1000 == 0:print i
        d.query = q
        try: 
            d.save()
        except django.db.utils.IntegrityError:
            continue
print i
