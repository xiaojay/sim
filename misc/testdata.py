#coding=utf-8
sentences = [
    u'小通', 
    u'哦，小通', 
    u'小通，你好！！',
    u'小通，哈哈！！',
]

import os, time, datetime, json, django
os.environ['DJANGO_SETTINGS_MODULE'] = 'sim.settings'
from sim.models import *
replace_words = [u'小黄鸡', u'小鸡鸡', u'小贱鸡',u'小鸡']

for d in Dialog.objects.order_by('?').all()[:100]:
    q = d.query
    for rw in replace_words:
        q = q.replace(rw, u'小通')
    sentences.append(q)
