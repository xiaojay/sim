#coding=utf-8
import sys, os, time, datetime, json, django
os.environ['DJANGO_SETTINGS_MODULE'] = 'sim.settings'
from django.db.models import Q
from sim.models import *

def now():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

def cmp(c1, c2):
    if c1.chat_at > c2.chat_at:
        return 1
    if c1.chat_at == c2.chat_at:
        if c1.created_at > c2.created_at:
            return 1
        else:
            return -1
    return -1

i = 0
print now()
users = [u.name for u in User.objects.filter(is_valid=True).all()]
print now(), 'load user list into memory'
Dialog.objects.all().delete()
for u in users:
    cs = [c for c in Chat.objects.filter( Q(user_from=u)| Q(user_to=u))]
    
    prev = None
    for c in sorted(cs, cmp=cmp):
        if prev and c.user_from == u'小黄鸡' and prev.user_to == u'小黄鸡' and (c.chat_at - prev.chat_at).seconds < 120 and c.content and prev.content:
            try:
                Dialog.objects.create(query_user=c.user_to, query=prev.content, answer=c.content, chat_at=c.chat_at)
            except django.db.IntegrityError:
                continue
        prev = c

    if i%1000 == 0:
       print now(), i
    i += 1
