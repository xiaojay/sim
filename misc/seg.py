#coding=utf-8
import os, time, datetime, json, django
from pyfudannlp import seg2
os.environ['DJANGO_SETTINGS_MODULE'] = 'sim.settings'
from sim.models import *
i = 0
q = Chat.objects.filter(segment=None)
for c in q.all():
    if i%10000 == 0:print i
    r = ''.join(seg2(c.content))
    print r
    Segment.objects.create(chat=c, content=r)
    i = i + 1
