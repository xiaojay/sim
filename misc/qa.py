#coding=utf-8
import os, time, datetime, json, django
os.environ['DJANGO_SETTINGS_MODULE'] = 'sim.settings'
from sim.models import *
from prep import prep

def now():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

filter_words =[u'24点', u'二十四点', u'+', u'来访', u'天气',]
replace_words = [u'小黄鸡', u'小鸡鸡', u'小贱鸡', u'小鸡']
remove_words_answer = [u'回复', u'呵呵']

Query.objects.all().delete()

i = 0
for d in Dialog.objects.all():
    q = d.query
    a = d.answer

    if not q: continue
    if a.find(u'回复') != -1:
        continue
    if a == u'呵呵':continue
    if a == u'。。。':continue

    is_filtered = False
    for w in filter_words:
        if q.find(w) != -1:
            is_filtered = True
            break
    if is_filtered:continue
    
    for rw in replace_words:
        q = q.replace(rw, u'小通')
        a = a.replace(rw, u'小通')
    q = prep(q)
    
    query, is_created = Query.objects.get_or_create(content=q)
    query.answer_set.get_or_create(content=a)
    
    if i%10000 == 0:
        print now(), i
    i += 1
