#coding=utf-8
import sys, os, time, datetime, json, django
os.environ['DJANGO_SETTINGS_MODULE'] = 'sim.settings'
from django.db.models import Q
from sim.models import *

def cmp(c1, c2):
    if c1.chat_at > c2.chat_at:
        return 1
    if c1.chat_at == c2.chat_at:
        if c1.created_at > c2.created_at:
            return 1
        else:
            return -1
    return -1

user = unicode(sys.argv[1], 'u8')

cs = [c for c in Chat.objects.filter( Q(user_from=user)| Q(user_to=user))]

prev = None
for c in sorted(cs, cmp=cmp):
    if prev:
        if c.user_from == u'小黄鸡' and prev.user_to == u'小黄鸡' and (c.chat_at - prev.chat_at).seconds < 120:
            try:
                Dialog.create(query_user=c.user_to, query=pre.content, answer=c.content, chat_at=c.chat_at)
            except django.db.IntegrityError:
                continue
    prev = c
