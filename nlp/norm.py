#coding=utf-8
import re

"""汉字处理的工具:
判断unicode是否是汉字，数字，英文，或者其他字符。
全角符号转半角符号。"""
def is_chinese(uchar):
    """判断一个unicode是否是汉字"""
    return uchar >= u'\u4e00' and uchar<=u'\u9fa5'

def is_number(uchar):
    """判断一个unicode是否是数字"""
    return uchar >= u'\u0030' and uchar<=u'\u0039'

def is_alphabet(uchar):
    """判断一个unicode是否是英文字母"""
    return (uchar >= u'\u0041' and uchar<=u'\u005a') or (uchar >= u'\u0061' and uchar<=u'\u007a')

def is_other(uchar):
    """判断是否非汉字，数字和英文字符"""
    return not (is_chinese(uchar) or is_number(uchar) or is_alphabet(uchar))

def b2q(uchar):
    """半角转全角"""
    inside_code=ord(uchar)
    if inside_code<0x0020 or inside_code>0x7e:      #不是半角字符就返回原来的字符
        return uchar
    if inside_code==0x0020: #除了空格其他的全角半角的公式为:半角=全角-0xfee0
        inside_code=0x3000
    else:
        inside_code+=0xfee0
    return unichr(inside_code)

def q2b(uchar):
    """全角转半角"""
    inside_code=ord(uchar)
    if inside_code==0x3000:
        inside_code=0x0020
    else:
        inside_code-=0xfee0
    if inside_code<0x0020 or inside_code>0x7e:      #转完之后不是半角字符返回原来的字符
        return uchar
    return unichr(inside_code)

def stringq2b(ustring):
    """把字符串全角转半角"""
    return "".join([q2b(uchar) for uchar in ustring])

#def uniform(ustring):
#    """格式化字符串，完成全角转半角，大写转小写的工作"""
#    return stringQ2B(ustring).lower()
def uniform(ustring):
    """格式化字符串，完成全角转半角，大写转小写的工作"""
    str_tmp = stringq2b(ustring).lower()
    return str_tmp
    #utmp= []
    #for u in str_tmp:
        #if(is_other(u)):
            #continue
        #utmp.append(u)
    #return ''.join(utmp).lower()

def string2List(ustring):
    """将ustring按照中文，字母，数字分开"""
    retList=[]
    utmp=[]
    lastchar=u'-'

    for uchar in ustring:
        if is_other(uchar):
            if len(utmp)==0:
                lastchar = uchar
                continue
            else:
                retList.append(uniform(''.join(utmp)))
                utmp=[]
                lastchar=u'-'
        else:
            if(is_other(lastchar) or (is_number(lastchar) == is_number(uchar)) and (is_chinese(lastchar) == is_chinese(uchar)) and (is_alphabet(lastchar) == is_alphabet(uchar))):
                utmp.append(uchar)
            else:
                if(len(utmp) == 0):
                    lastchar = uchar
                    continue
                else:
                    retList.append(uniform(''.join(utmp)))
                    utmp=[uchar]
        lastchar = uchar
    if len(utmp)!=0:
        retList.append(uniform(''.join(utmp)))
    return retList

def chinese_to_num(strnum):
    num={u'一':'1',u'二':'2',u'三':'3',u'四':'4',u'五':'5',u'六':'6',u'七':'7',u'八':'8',u'九':'9',u'零':'0','十':'','百':'','千':'','万':''}
    res = ''
    trans = 0
    for s in strnum:
        if(s in num):
            res += num[s]
            trans = 1
        else:
            res += s
    return res if trans else strnum

def is_chineseNum(strnum):
    chineseNum = u'^([第]?)([零一二三四五六七八九十百千万]+)([部季集]?)$'
    re_chineseNum = re.compile(chineseNum)
    m = re_chineseNum.match(strnum)
    if(m):
        strnum = m.group(2)
        if(strnum[0] == u'十'):
            return '1'+chinese_to_num(strnum[1:])
        else:
            return chinese_to_num(strnum)
    else:
        return strnum

def num_to_chinese(strnum):
    num={'1':u'一','2':u'二','3':u'三','4':u'四','5':u'五','6':u'六','7':u'七','8':u'八','9':u'九','0':u'零'}
    res = ''
    trans = 0
    for s in strnum:
        if(s in num):
            res += num[s]
            trans = 1
        else:
            res += s
    return res if trans else strnum

if __name__=="__main__":

    #test Q2B and B2Q
    #for i in range(0x0020,0x007F):
    #        print Q2B(B2Q(unichr(i))).encode('gb2312', 'ignore'),B2Q(unichr(i)).encode('gb2312', 'ignore')
    str_int = u'第二季'
    print is_chineseNum(str_int)
    ustring=u'中国 人名 maggie.Q maggieQ, 江南style'
    ret=string2List(ustring)
    for r in ret:
        print r.encode('utf-8', 'ignore')

