#coding=utf-8
from nlp import uniform

def remove_punctuation(s):
    punctuation = [
        '.', ',', '?', '!', ';', '&', '*', '%', '+', '=', '-', '~', '$' , '(', ')', '@', '^', '"', '>', '_', '|', '[', ']', 
        u'，', u'。', u'！', u'？', u'：', u'‘', u'“', u'”', u'、', u'【', u'】', u'…',
        u'啊', u'阿', u'呀', u'啦', u'吧', u'把', u'巴', u'呵', u'哦', u'哈', u'亲', u'额', u'呃', u'嘿',
        u'喂', u'哇', u'呢', u'嗯', u'呦']
   
    while True:
        if s and s[-1] in punctuation:
            s = s[:-1]
        else:
            break
    while True:
        if s and s[0] in punctuation:
            s = s[1:]
        else:
            break
    s = s.strip()
    return s

def remove_name(s):
    name = u'小通'
    n = len(name)
    while True:
        if s and s[-n:]== name:
            s = s[:-n]
        else:
            break
    while True:
        if s and s[:n]==name:
            s = s[n:]
        else:
            break
    s = s.strip()
    return s
    

def prep(s):
    s = s.strip()
    #全角 --> 半角
    #大写 --> 小写
    s = uniform(s)
    
    s2 = remove_punctuation(s)
    if s2:
        s = s2
    s2 = remove_name(s)
    if s2:
        s = s2
    s2 = remove_punctuation(s)
    if s2:
        s = s2
    return s

if __name__ == '__main__':
    from testdata import sentences
    for s in sentences:
        s2 = prep(s)
        print s
        print '==>', s2, '\n'


