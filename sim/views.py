#coding=utf-8
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from models import Query, Missed, Receive
from nlp import prep
def home(request):
    return render_to_response('home.html')

def user(rquest):
    pass

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def query(request):
    query = request.GET.get('query', '')
    if not query:
        return HttpResponse('No INPUT')
    ip = get_client_ip(request)

    query = prep(query)
    q =  Query.objects.filter(content=query)
    if q.count() >= 1:
        r = q.all()[0]
    else:
        q = Query.objects.filter(content__startswith=query)
        if q.count() > 1:
            r = q.all()[0]
        else:
            q2 = Query.objects.filter(content__contains=query)
            if q2.count() == 0:
                Missed.objects.create(content=query)
                Receive.objects.create(content=query, reply='Missed', ip=ip)
                return HttpResponse('Missed')
            r = q2.order_by('?')[0]

    a = r.answer_set.order_by('?')[0].content
    Receive.objects.create(content=query, reply=a, ip=ip)
    return HttpResponse(a.encode('u8'))

def train(request):
    input_ = request.POST.get('input_', '')
    if not input_:
        return HttpResponse('No INPUT')
