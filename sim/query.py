#coding=utf-8
from models import Query, Missed

def _query(q, is_train=False):
    query = request.GET.get('query', '')
    if not query:
        return HttpResponse('No INPUT')
    query = prep(query)
    q = Query.objects.filter(content__startswith=query)
    if q.count() > 1:
        r = Query.objects.filter(content=query)[0]
    elif q.count() == 1:
        r = q.all()[0]
    else:
        q2 = Query.objects.filter(content__contains=query)
        if q2.count() == 0:
            Missed.objects.create(content=query)
            return HttpResponse('Missed')
        r = q2.order_by('?')[0]

    a = r.answer_set.order_by('?')[0].content
    return HttpResponse(a.encode('u8'))
