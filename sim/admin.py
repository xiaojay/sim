#coding=utf-8
from django.contrib import admin
from models import *

class ChatAdmin(admin.ModelAdmin):
    search_fields = ['content']
    #list_filter = ['is_strict', 'is_enabled']

class DialogAdmin(admin.ModelAdmin):
    search_fields = ['query']

class UserAdmin(admin.ModelAdmin):
    list_filter = ['is_valid', ]

class ReceiveAdmin(admin.ModelAdmin):
    list_display = ['created_at', 'content', 'reply', 'ip' ]

class AnswerInline(admin.StackedInline):
    model = Answer

class QueryAdmin(admin.ModelAdmin):
    inlines = [
        AnswerInline,
    ]
    search_fields = ['content']

class MissedAdmin(admin.ModelAdmin):
    search_fields = ['content']

admin.site.register(Chat, ChatAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Dialog, DialogAdmin)
admin.site.register(Query, QueryAdmin)

admin.site.register(Missed, MissedAdmin)
admin.site.register(Receive, ReceiveAdmin)








