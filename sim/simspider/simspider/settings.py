# Scrapy settings for sim project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'simspider'
USER_AGENT = 'Mozilla/5.0 (X11; U; Linux i686; zh-CN; rv:1.9.1.7) Gecko/20100106 Ubuntu/9.10 (karmic) Firefox/3.5.7'

SPIDER_MODULES = ['simspider.spiders']
NEWSPIDER_MODULE = 'simspider.spiders'

ITEM_PIPELINES = [
    'simspider.pipelines.ChatPipeline',
]


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'sim (+http://www.yourdomain.com)'

def setup_django_env(path):
    import imp, os, sys
    from django.core.management import setup_environ

    f, filename, desc = imp.find_module('settings', [path])
    project = imp.load_module('settings', f, filename, desc)

    setup_environ(project)
    sys.path.append(os.path.abspath(os.path.join(path, os.path.pardir)))

setup_django_env('/Users/jay/py/sim/sim')
#('/home/jay/websites/hzlife/hzlife')
