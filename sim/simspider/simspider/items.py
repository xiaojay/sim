#coding=utf-8
# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html
from scrapy.contrib.djangoitem import DjangoItem
from sim.models import Chat

class ChatItem(DjangoItem):
    django_model = Chat
