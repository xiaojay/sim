#coding=utf-8
import re, time
from scrapy import log
from scrapy.http import Request, FormRequest
from scrapy.spider import BaseSpider
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from simspider.items import ChatItem

def print_red(s):print '\033[0;31;48m%s\033[0m'%s

class SimSpider(BaseSpider):
    name = 'sim_spider'

    def __init__(self, status_id='4515299360', page='0'):
        self.status_id = status_id
        url = 'http://page.renren.com/601621937/fdoing/%s?curpage=%s'
        self.status_url = 'http://page.renren.com/601621937/fdoing/%s'%status_id
        self.start_urls = [url%(status_id, page)]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        try:
            cl = hxs.select('//dl[@id="comment_list"]')[0]
        except IndexError:
            open('t.html', 'w').write(response.body)

        cs = cl.select('dd')
        r = []
        for c in cs:
            i = ChatItem()
            info = c.select('div[@class="info"]')[0]
            #print info.extract()
            i['status_id'] = self.status_id
            i['user_from'] = info.select('a/@toname').extract()[0]
            i['chat_at'] = info.select('span/text()').extract()[0].strip()
            try:
                reply = c.select('div[@class="reply"]/p/span/text()').extract()[0]
            except IndexError:
                continue

            #print type(reply)
            if reply.find(u'回复') != -1:
                if reply.find(':') != -1:
                    spliter = u':'
                else:
                    spliter = u'：'
                i['user_to'] = reply.split(spliter)[0][2:].strip()
                i['content'] = reply.split(spliter)[-1].strip()
            else:
                i['user_to'] = u'小黄鸡'
                i['content'] = reply
            print i['user_from']
            print i['user_to']
            print i['content']
            print i['chat_at']
            print
            yield i

        next = hxs.select('//a[@title="%s"]/@href'%u'下一页')
        if next:
            next_url = self.status_url + next[0].extract()
            print_red(next_url)
            time.sleep(0.5)
            yield Request(next_url)






