#coding=utf-8
from django.db import models

class Receive(models.Model):
    content = models.CharField(max_length=500)
    reply = models.CharField(max_length=500, blank=True, null=True)
    ip = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.content[:50]

class Chat(models.Model):
    class Meta:
        verbose_name = u'抓取记录'
        verbose_name_plural = verbose_name

    status_id = models.CharField(max_length=50)
    user_from = models.CharField(max_length=50)
    user_to = models.CharField(max_length=50)
    content = models.CharField(max_length=500)
    chat_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.content[:20]

class Segment(models.Model):
    class Meta:
        verbose_name = u'分词'
        verbose_name_plural = verbose_name

    chat = models.ForeignKey(Chat)
    content = models.CharField(max_length=600)
    def __unicode__(self):
        return self.content[:25]

class User(models.Model):
    name = models.CharField(max_length=50, db_index=True, unique=True)
    is_valid = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

class Dialog(models.Model):
    class Meta:
        verbose_name = u'合并的对话'
        verbose_name_plural = verbose_name
        unique_together = ('query_user', 'query', 'chat_at')

    query_user = models.CharField(max_length=50)
    query = models.CharField(max_length=50)
    answer = models.CharField(max_length=50)

    chat_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.query[:20]

class Query(models.Model):
    class Meta:
        verbose_name = u'query'
        verbose_name_plural = verbose_name

    content = models.CharField(max_length=500, unique=True, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.content[:20]

class Answer(models.Model):
    class Meta:
        verbose_name = u'answer'
        verbose_name_plural = verbose_name

    content = models.CharField(max_length=500)
    query = models.ForeignKey(Query)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.content[:20]

class Missed(models.Model):
    class Meta:
        verbose_name = u'Missed'
        verbose_name_plural = verbose_name

    content = models.CharField(max_length=500, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.content[:20]
